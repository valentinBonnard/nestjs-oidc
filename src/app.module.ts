import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OidcClientModule } from './oidc-client/oidc-client.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { ConfigurationService } from './configuration/configuration.service';

@Module({
  imports: [
    OidcClientModule.registerAsync({
      imports: [ConfigurationModule.register({ folder: './config' })],
      useExisting: ConfigurationService,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
