import { Module, DynamicModule, Provider } from '@nestjs/common';
import { ConfigurationService } from './configuration.service';
import { CONFIGURATION_OPTIONS } from './configuration.constants';
import { ConfigurationModuleOptions } from './interfaces';

@Module({
  providers: [ConfigurationService],
  exports: [ConfigurationService],
})
export class ConfigurationModule {
  static register(options: ConfigurationModuleOptions): DynamicModule {
    return {
      module: ConfigurationModule,
      providers: [
        {
          provide: CONFIGURATION_OPTIONS,
          useValue: options,
        },
        ConfigurationService,
      ],
      exports: [CONFIGURATION_OPTIONS],
    };
  }
}
