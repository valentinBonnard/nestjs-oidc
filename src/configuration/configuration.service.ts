import { Injectable, Inject } from '@nestjs/common';
import * as path from 'path';
import * as dotenv from 'dotenv';
import * as fs from 'fs';

import { CONFIGURATION_OPTIONS } from './configuration.constants';
import {
  ConfigurationModuleOptions,
  EnvironmentConfiguration,
} from './interfaces';
import { OidcModuleOptions, OidcOptionsFactory } from '../oidc-client/interfaces';

@Injectable()
export class ConfigurationService implements OidcOptionsFactory {
  private readonly envConfiguration: EnvironmentConfiguration;

  constructor(
    @Inject(CONFIGURATION_OPTIONS) options: ConfigurationModuleOptions,
  ) {
    const filePath = `${process.env.NODE_ENV || 'development'}.env`;
    const envFile = path.resolve(__dirname, '../../', options.folder, filePath);
    this.envConfiguration = dotenv.parse(fs.readFileSync(envFile));
  }

  createOidcClientFactory(): OidcModuleOptions {
    return {
        test: this.envConfiguration.TEST,
    };
  }
}
