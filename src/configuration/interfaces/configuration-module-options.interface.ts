export interface ConfigurationModuleOptions {
    folder: string;
}

export interface EnvironmentConfiguration {
    [key: string]: string;
}
