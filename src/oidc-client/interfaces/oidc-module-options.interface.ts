import { ModuleMetadata, Type } from '@nestjs/common/interfaces';

export interface OidcModuleOptions {
    test: string;
}

export interface OidcOptionsFactory {
    createOidcClientFactory(): Promise<OidcModuleOptions> | OidcModuleOptions;
}

export interface OidcModuleAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
    useExisting?: Type<OidcOptionsFactory>;
    useClass?: Type<OidcOptionsFactory>;
    useFactory?: (...args: any[]) => Promise<OidcModuleOptions> | OidcModuleOptions;
    inject?: any[];
}
