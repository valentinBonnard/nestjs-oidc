import { Module, DynamicModule, Provider } from '@nestjs/common';

import { OidcClientService } from './oidc-client.service';
import { createOidcClientProvider } from './oidc-client.provider';
import { OIDC_CLIENT_MODULE_OPTIONS } from './oidc-client.contants';
import {
  OidcOptionsFactory,
  OidcModuleOptions,
  OidcModuleAsyncOptions,
} from './interfaces';

@Module({
  providers: [OidcClientService],
  exports: [OidcClientService],
})
export class OidcClientModule {
  static register(options: OidcModuleOptions): DynamicModule {
    return {
      module: OidcClientModule,
      providers: createOidcClientProvider(options),
    };
  }

  static registerAsync(options: OidcModuleAsyncOptions): DynamicModule {
    return {
      module: OidcClientModule,
      imports: options.imports || [],
      providers: this.createAsyncProviders(options),
    };
  }

  private static createAsyncProviders(
    options: OidcModuleAsyncOptions,
  ): Provider[] {
    if (options.useExisting || options.useFactory) {
      return [this.createAsyncOptionsProviders(options)];
    }

    return [
      this.createAsyncOptionsProviders(options),
      {
        provide: options.useClass,
        useClass: options.useClass,
      },
    ];
  }

  private static createAsyncOptionsProviders(
    options: OidcModuleAsyncOptions,
  ): Provider {
    if (options.useFactory) {
      return {
        provide: OIDC_CLIENT_MODULE_OPTIONS,
        useFactory: options.useFactory,
        inject: options.inject || [],
      };
    }
    return {
      provide: OIDC_CLIENT_MODULE_OPTIONS,
      useFactory: async (optionsFactory: OidcOptionsFactory) =>
        await optionsFactory.createOidcClientFactory(),
      inject: [options.useExisting || options.useClass],
    };
  }
}
