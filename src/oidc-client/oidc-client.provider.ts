import { OIDC_CLIENT_MODULE_OPTIONS } from './oidc-client.contants';

export function createOidcClientProvider(options): any {
    return [{provide: OIDC_CLIENT_MODULE_OPTIONS, useValue: options || {} }];
}
