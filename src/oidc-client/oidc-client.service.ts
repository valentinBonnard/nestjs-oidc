import { Injectable, Inject } from '@nestjs/common';

import { OidcModuleOptions } from './interfaces';
import { OIDC_CLIENT_MODULE_OPTIONS } from './oidc-client.contants';

@Injectable()
export class OidcClientService {
    private test: OidcModuleOptions;

    constructor(@Inject(OIDC_CLIENT_MODULE_OPTIONS) private readonly options: OidcModuleOptions) {
        console.log('*****************', this.options); // We get { test: 'test' } => :)
    }
}
